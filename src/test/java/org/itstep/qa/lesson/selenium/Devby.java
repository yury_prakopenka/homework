package org.itstep.qa.lesson.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Devby {
    private WebDriver driver;

    @BeforeClass
    public void createBrowser() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @AfterClass
    public void quitBrowser(){
        driver.quit();
    }

    @BeforeMethod
    public void clear() {
        driver.manage().deleteAllCookies();
    }

    @Test
    public void checkEmail() throws InterruptedException {
        driver.get("https://dev.by/");
        WebElement webElement = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[3]/header/div[1]/div[2]/div[1]/div/a"));
        webElement.click();

        webElement = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/ul/a[2]"));
        webElement.click();

        webElement = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/div[1]/label[1]/div"));
        webElement.click();

        webElement = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[2]/div/input"));
        webElement.sendKeys("qwerty");

        Thread.sleep(2000);
        webElement = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/button"));
        webElement.click();

        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/span")).getText(), "Введите корректный адрес электронной почты.");

    }

    @Test
    public void checkPass() throws InterruptedException {
        driver.get("https://dev.by/");
        WebElement webElement = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[3]/header/div[1]/div[2]/div[1]/div/a"));
        webElement.click();

        webElement = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/ul/a[2]"));
        webElement.click();

        webElement = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/div[1]/label[1]/div"));
        webElement.click();

        webElement = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[1]/input"));
        webElement.sendKeys("7302438@gmail.com");

        Thread.sleep(2000);
        webElement = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/button"));
        webElement.click();

        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/span")).getText(), "Введите пароль.");

    }

    @Test
    public void checkCorrectEmail() throws InterruptedException {
        driver.get("https://dev.by/");
        WebElement webElement = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[3]/header/div[1]/div[2]/div[1]/div/a"));
        webElement.click();

        webElement = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/ul/a[2]"));
        webElement.click();

        webElement = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/div[1]/label[1]/div"));
        webElement.click();

        webElement = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[1]/input"));
        webElement.sendKeys("7302438");

        Thread.sleep(2000);
        webElement = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/button"));
        webElement.click();

        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/span")).getText(), "Введите корректный адрес электронной почты.");

    }

}
